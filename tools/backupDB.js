var fs = require('fs');
const MongoClient = require('mongodb').MongoClient;

const dirName = __dirname + "/bkup"
var listFile = []

let filelist = fs.readdirSync(dirName)
listFile.push(filelist.length)

filelist.forEach(file => {
  let id = parseInt(file[0])
  if (id >= 7) {
    fs.unlink(dirName + "/" + file, err => {
      if (err) console.log("Warning! Cannot remove", file)
    })
  } else {
    let oldName = dirName + "/" + file
    let newName = dirName + "/" + (id + 1) + file.slice(1, file.length)
    fs.rename(oldName, newName, err => {
      if (err) console.log("Warning! Cannot rename", file)
    })
  }
})


var date = new Date()
var logName = dirName + "/1_data-bkup_" + ("0" + date.getHours()).slice(-2) + ("0" + date.getMinutes()).slice(-2) + ("0" + date.getSeconds()).slice(-2)
  + ("0" + date.getDate()).slice(-2) + ("0" + (date.getMonth()+1)).slice(-2) + (date.getFullYear() % 100)
console.log(logName)

const uri = "mongodb+srv://bookTracker_web:tra1331995@booktracker.49gl4.mongodb.net/bookTracker?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

// fs.open(logName, 'w', (err, fd) => {
  console.log("File ready")
  client.connect((err, cl) => {
    if (err) throw err
    const db = cl.db("bookTracker")
    db.listCollections().toArray((err, cols) => {
      var counter = cols.length
      var offset = 0
      fs.appendFileSync(logName, '{"xx":"x"');
      cols.forEach((itCol, id) => {
        let collection = db.collection(itCol.name)
        let collection_name = itCol.name;
        collection.find().toArray((err, doc) => {
          let buffer = ',"'+collection_name+'":'
            +JSON.stringify(doc)
          fs.appendFile(logName, buffer, err => {
            if(err) throw err
            counter--
            if (!counter) {
              fs.appendFileSync(logName, '}');
              console.log("Close DB at " + itCol.name)
              cl.close()
            }
          })
        })
      })
    })
  })
// })
