if (process.argv.length < 3) {
  throw "Missing data source"
}

var fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

var sourceDB 
// sourceDB = fs.readFileSync(process.argv[2], 'utf-8');
sourceDB = '{"books":[{"_id":"601304aef870c9930e497e71","name":"Mar Heaven","pages":161,"start_date":"1v8Cb5","start_page":98,"rec":["6013079ccb91744db9c9f67a","601307d6cb91744db9c9f67b","601307e5cb91744db9c9f67c","601307ffcb91744db9c9f67d","6013080bcb91744db9c9f67e","60130815cb91744db9c9f67f","60130823cb91744db9c9f680","6013083dcb91744db9c9f681","60130847cb91744db9c9f682","60130852cb91744db9c9f683"]},{"_id":"601304ccf870c9930e499057","name":"Master JavaScript","pages":250,"start_date":"1sl5A3","start_page":143,"rec":["6013095bcb91744db9c9f692","60130965cb91744db9c9f693","6013096dcb91744db9c9f694","60130977cb91744db9c9f695","60130980cb91744db9c9f696","60130987cb91744db9c9f697","6013098ecb91744db9c9f698","60130996cb91744db9c9f699"]},{"_id":"601304e6f870c9930e499d2e","name":"Mastering JavaScript Object-Oriented Programming","pages":287,"start_date":"1tGH3I","start_page":19,"rec":["60130876cb91744db9c9f684","6013087fcb91744db9c9f685","6013088acb91744db9c9f686","60130893cb91744db9c9f687","601308a0cb91744db9c9f688","601308a9cb91744db9c9f689","601308b7cb91744db9c9f68a","601308c6cb91744db9c9f68b","601308d4cb91744db9c9f68c","601308e1cb91744db9c9f68d","601308f4cb91744db9c9f68e","601308ffcb91744db9c9f68f","60130910cb91744db9c9f690","601ce82844ab620546168f64","601cecb844ab620546168f65","601cf06244ab620546168f66","601cfab344ab620546168f67","601d054244ab620546168f68","601d087444ab620546168f69","6020a4b328a2462b7a811d73","6020a8ce28a2462b7a811d74","6020ab1528a2462b7a811d75","6020ac6b28a2462b7a811d76","6020afae28a2462b7a811d77","6020afd428a2462b7a811d78","6020b02828a2462b7a811d7b","6020b2e428a2462b7a811d7c","6020c18628a2462b7a811d7f","6020c3cf28a2462b7a811d80","6020fb0e28a2462b7a811d81","6020fce128a2462b7a811d82","6020ff1e28a2462b7a811d83","6021009428a2462b7a811d84","6021026928a2462b7a811d85","6021042728a2462b7a811d86","602104f128a2462b7a811d87","6021077c28a2462b7a811d88","6021082628a2462b7a811d89","60210f0528a2462b7a811d8a","602116b528a2462b7a811d8c"]},{"_id":"60229a106a63e26f79bb9b34","rec":[],"name":"Measure What Matters","pages":297,"start_page":1,"start_date":"1tTmRV","__v":0},{"_id":"6024197abe9c5652d1e27231","name":"Ahihi","pages":100,"__v":0}],"booksRead":[{"_id":"6028110d3ed9c71d25fe2310","sections":[],"bookId":"6024197abe9c5652d1e27231","user":"tester","__v":0}],"records":[{"_id":"6013079ccb91744db9c9f67a","type":1,"date":"1sqGVQ","page":100,"__v":0},{"_id":"601307d6cb91744db9c9f67b","type":1,"date":"1sqGdb","page":105,"__v":0},{"_id":"601307e5cb91744db9c9f67c","type":1,"date":"1sqGmr","page":110,"__v":0},{"_id":"601307ffcb91744db9c9f67d","type":1,"date":"1sqGtn","page":116,"__v":0},{"_id":"6013080bcb91744db9c9f67e","type":1,"date":"1sqH61","page":125,"__v":0},{"_id":"60130815cb91744db9c9f67f","type":0,"date":"1sqHA4","page":127,"__v":0},{"_id":"60130823cb91744db9c9f680","type":1,"date":"1sqMpU","page":127,"__v":0},{"_id":"6013083dcb91744db9c9f681","type":1,"date":"1sqNFX","page":141,"__v":0},{"_id":"60130847cb91744db9c9f682","type":1,"date":"1sqNn6","page":160,"__v":0},{"_id":"60130852cb91744db9c9f683","type":0,"date":"1sqNrv","page":161,"__v":0},{"_id":"60130876cb91744db9c9f684","type":1,"date":"1tGOkc","page":19,"__v":0},{"_id":"6013087fcb91744db9c9f685","type":1,"date":"1tGP6Y","page":23,"__v":0},{"_id":"6013088acb91744db9c9f686","type":0,"date":"1tGP8r","page":24,"__v":0},{"_id":"60130893cb91744db9c9f687","type":1,"date":"1tMTin","page":24,"__v":0},{"_id":"601308a0cb91744db9c9f688","type":1,"date":"1tMU2A","page":29,"__v":0},{"_id":"601308a9cb91744db9c9f689","type":1,"date":"1tMULy","page":33,"__v":0},{"_id":"601308b7cb91744db9c9f68a","type":0,"date":"1tMUcH","page":38,"__v":0},{"_id":"601308c6cb91744db9c9f68b","type":1,"date":"1tOOHs","page":39,"__v":0},{"_id":"601308d4cb91744db9c9f68c","type":0,"date":"1tOOvJ","page":43,"__v":0},{"_id":"601308e1cb91744db9c9f68d","type":1,"date":"1tOgEm","page":43,"__v":0},{"_id":"601308f4cb91744db9c9f68e","type":0,"date":"1tOgRs","page":49,"__v":0},{"_id":"601308ffcb91744db9c9f68f","type":1,"date":"1tOkto","page":50,"__v":0},{"_id":"60130910cb91744db9c9f690","type":0,"date":"1tOlQZ","page":58,"__v":0},{"_id":"6013095bcb91744db9c9f692","type":1,"date":"1sl70B","page":144,"__v":0},{"_id":"60130965cb91744db9c9f693","type":1,"date":"1slHBo","page":148,"__v":0},{"_id":"6013096dcb91744db9c9f694","type":1,"date":"1slRdC","page":150,"__v":0},{"_id":"60130977cb91744db9c9f695","type":1,"date":"1slhxa","page":182,"__v":0},{"_id":"60130980cb91744db9c9f696","type":1,"date":"1slp7K","page":200,"__v":0},{"_id":"60130987cb91744db9c9f697","type":1,"date":"1sm0sN","page":217,"__v":0},{"_id":"6013098ecb91744db9c9f698","type":1,"date":"1sm3kS","page":220,"__v":0},{"_id":"60130996cb91744db9c9f699","type":0,"date":"1sm683","page":250,"__v":0},{"_id":"601ce82844ab620546168f64","type":1,"date":"1tS9Am","page":59,"__v":0},{"_id":"601cecb844ab620546168f65","type":0,"date":"1tS9To","page":62,"__v":0},{"_id":"601cf06244ab620546168f66","type":1,"date":"1tS9jD","page":62,"__v":0},{"_id":"601cfab344ab620546168f67","type":1,"date":"1tSARU","page":76,"__v":0},{"_id":"601d054244ab620546168f68","type":1,"date":"1tSBAu","page":82,"__v":0},{"_id":"601d087444ab620546168f69","type":0,"date":"1tSBOD","page":82,"__v":0},{"_id":"6020a4b328a2462b7a811d73","type":1,"date":"1tTDyC","page":82,"__v":0},{"_id":"6020a8ce28a2462b7a811d74","type":1,"date":"1tTEGQ","page":88,"__v":0},{"_id":"6020ab1528a2462b7a811d75","type":1,"date":"1tTEQ0","page":91,"__v":0},{"_id":"6020ac6b28a2462b7a811d76","type":1,"date":"1tTEVa","page":94,"__v":0},{"_id":"6020afae28a2462b7a811d77","type":1,"date":"1tTEjI","page":100,"__v":0},{"_id":"6020afd428a2462b7a811d78","type":0,"date":"1tTEjt","page":0,"__v":0},{"_id":"6020b02828a2462b7a811d7b","type":1,"date":"1tTElH","page":101,"__v":0},{"_id":"6020b2e428a2462b7a811d7c","type":0,"date":"1tTEwk","page":0,"__v":0},{"_id":"6020c18628a2462b7a811d7f","type":1,"date":"1tTFxA","page":105,"__v":0},{"_id":"6020c3cf28a2462b7a811d80","type":0,"date":"1tTG7k","page":110,"__v":0},{"_id":"6020fb0e28a2462b7a811d81","type":1,"date":"1tTJub","page":110,"__v":0},{"_id":"6020fce128a2462b7a811d82","type":1,"date":"1tTK3H","page":114,"__v":0},{"_id":"6020ff1e28a2462b7a811d83","type":1,"date":"1tTKCe","page":120,"__v":0},{"_id":"6021009428a2462b7a811d84","type":0,"date":"1tTKIm","page":0,"__v":0},{"_id":"6021026928a2462b7a811d85","type":1,"date":"1tTKQT","page":122,"__v":0},{"_id":"6021042728a2462b7a811d86","type":0,"date":"1tTKXn","page":0,"__v":0},{"_id":"602104f128a2462b7a811d87","type":1,"date":"1tTKb6","page":123,"__v":0},{"_id":"6021077c28a2462b7a811d88","type":1,"date":"1tTKll","page":126,"__v":0},{"_id":"6021082628a2462b7a811d89","type":0,"date":"1tTKoZ","page":0,"__v":0},{"_id":"60210f0528a2462b7a811d8a","type":1,"date":"1tTLIO","page":128,"__v":0},{"_id":"602116b528a2462b7a811d8c","type":0,"date":"1tTLoe","page":134,"__v":0}],"sectionRecords":[],"users":[{"_id":"600eff2ca25f6113a7eaacf4","userID":"xuantra","email":"jurytare@yahoo.com.vn","password":"$2a$08$3MpHnTMMmuLlS/F61TndcuK5fO4SkHUL9Dlo7rgi1UIQ.cJuRPRRO","role":1,"bookList":["601304aef870c9930e497e71","601304ccf870c9930e499057","601304e6f870c9930e499d2e","6024197abe9c5652d1e27231"],"__v":0},{"_id":"602293a46a63e26f79bb9b33","bookList":["60229a106a63e26f79bb9b34"],"userID":"Queennie1310","email":"buithuyenqn@gmail.com","password":"$2a$08$IOL68DFoHvGqso7sxX4f3OABX0.sU4YHylb54W3i1WcUbS777epCm","role":1,"__v":0},{"_id":"6024e550c6935f454c2310c8","bookList":["6028110d3ed9c71d25fe2310"],"userID":"tester","email":"tester@mrtea.vn","password":"$2a$08$hK3LyCzt2przicI9eUjFNOGw.5Y/pcysp4U6SnkvaZRA1RvOloik2","role":1,"__v":0}]}';

sourceDB = JSON.parse(sourceDB);

for(let colName in sourceDB) {
  sourceDB[colName].forEach(obj => {
    obj._id = new ObjectId(obj._id);
  })
  // sourceDB[colName]._id = new ObjectId(sourceDB[colName]._id)
  // console.log("Convert" , colName, sourceDB[colName]._id)
}

for(let i in sourceDB.books) {
  for(let j in sourceDB.books[i].rec){
    sourceDB.books[i].rec[j] = new ObjectId(sourceDB.books[i].rec[j])
  }
}

for(let i in sourceDB.booksRead) {
  sourceDB.booksRead[i].bookId = new ObjectId(sourceDB.booksRead[i].bookId);
  for(let j in sourceDB.booksRead[i].sections){
    sourceDB.booksRead[i].sections[j] = new ObjectId(sourceDB.booksRead[i].sections[j])
  }
}

for(let i in sourceDB.users) {
  for(let j in sourceDB.users[i].bookList){
    sourceDB.users[i].bookList[j] = new ObjectId(sourceDB.users[i].bookList[j])
  }
}

let currentDB = {};
let logName = "currentDB-" + (new Date).getTime().toString();

const uri = "mongodb+srv://bookTracker_web:tra1331995@booktracker.49gl4.mongodb.net/bookTracker?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

function processFile(err) {
  if (err) throw err
}

client.connect((err, cl) => {
  if (err) throw err;
  const db = cl.db("bookTracker")
  db.listCollections().toArray((err, cols) => {
    var counter = cols.length
    fs.appendFileSync(logName, '{"xx":"x"');
    cols.forEach((itCol, id) => {
      let collection = db.collection(itCol.name)
      let collection_name = itCol.name;
      collection.find().toArray((err, doc) => {
        let buffer = ',"' + collection_name + '":'
          + JSON.stringify(doc)
        fs.appendFile(logName, buffer, err => {
          if (err) throw err
          counter--
          if (!counter) {
            fs.appendFileSync(logName, '}');
            
            console.log("Finish backup at", logName);
            console.log("Start erasing...");
            counter = cols.length;
            cols.forEach(colName => {
              console.log("Droping", colName.name)
              db.dropCollection(colName.name, err => {
                if(err) throw err;
                counter--
                if (!counter) {
                  console.log("Start restoring...");

                  for(var key in sourceDB) {
                    counter++;
                    let collectionName = key;
                    console.log("Restoring", collectionName)
                    db.createCollection(collectionName, (err, collection) => {
                      if(err) throw err;
                      if(sourceDB[collectionName].length){
                        collection.insertMany(sourceDB[collectionName], (err, res) => {
                          if(err) throw err;
                          counter--;
                          if(!counter) {
                            cl.close();
                          }
                        })
                      }else{
                        counter--;
                        if(!counter) {
                          cl.close();
                        }
                      }
                    })
                  }
                }
              });
            })
          }
        })
      })
    })
  })
})

fs.appendFileSync(logName, '}');

// var db, clg;

// client.connect()
//   .then(cl => {
//     clg = cl;
//     db = cl.db("bookTracker");
//     return db.listCollections().toArray()
//   })
//   .then(cols => {
//     let workPromises = [];
//     fs.appendFileSync(logName, '{"xx":"x"');
//     cols.forEach((itCol, id) => {
//       let collection = db.collection(itCol.name)
//       workPromises.push([collection.find().toArray(), itCol.name]);
//     })
//     return Promise.all(workPromises);
//   })
//   .then(([doc, collection_name]) => {
//     let buffer = ',"' + collection_name + '":'
//     + JSON.stringify(doc)
//     return fs.promises.appendFile(logName, buffer)
//   })
//   .then(processFile)
//   .finally(() => {
    
//     console.log('Finished');
//     clg.close();
//   })
//   .catch(err => {
//     console.log(err);
//   });



