let Book = require('../server/models/book.model');
let BookRead = require('../server/models/bookRead.model');
const SectionRecord = require('../server/models/sectionRecord.model');
const User = require('../server/models/user.model');
const { decodeNum } = require('../server/utils/utils');
let {
    books,
    users,
    records,
} = require('./data.json');

const ReadingState = {
    IDLE: 0,
    READING: 1,
    PAUSED: 2,
    RESUME: 3,
    ROTATE: 4,
}

var db = require('../server/models')

db.init()
    .then(() => {
        console.log("Successfully connect to MongoDB.");
        let proms = []
        users.forEach(usr => {
            let newUser = new User();
            newUser.uid = usr.userID;
            newUser.email = usr.email;
            newUser.password = usr.password;
            newUser.role = usr.role;
            proms.push(newUser.save());
            console.log(usr.userID);
            if (usr.hasOwnProperty('bookList')) {
                usr.bookList.forEach(bid => {
                    let book = books.find(bInfo => bInfo._id === bid);
                    if (book) {
                        console.log(book.name);
                        let newBook = new Book();
                        newBook.name = book.name;
                        newBook.author = book.author;
                        newBook.pages = book.pages;
                        newBook.usage = book.usage || 1;
                        newBook.user = usr.userID;
                        newBook.dateCreated = book.dateCreated || decodeNum(book.start_date);
                        proms.push(newBook.save());
                        
                        let rbModel = new BookRead();
                        rbModel.bid = newBook._id;
                        rbModel.pages = book.pages;
                        rbModel.user = usr.userID;
                        rbModel.sections = [];
                        let section = undefined;
                        if (Array.isArray(book.rec) && book.rec.length) {
                            book.rec.forEach(rid => {
                                let rec = records.find(r => r._id === rid);
                                if (rec) {
                                    rec.date = decodeNum(rec.date);
                                    console.log(rec.date + ' - ' + rec.type + ' - ' + rec.page);
                                    if (section) {
                                        let record = {
                                            _id: rec._id,
                                            time: rec.date - section.date,
                                            page: rec.page,
                                        }
                                        if (rec.type) {
                                            if (rec.page) {
                                                record.state = ReadingState.READING;
                                            } else {
                                                record.state = ReadingState.RESUME;
                                            }
                                        } else {
                                            if (rec.page) {
                                                record.state = ReadingState.IDLE;
                                            } else {
                                                record.state = ReadingState.PAUSED;
                                            }
                                        }
                                        section.recs.push(record);
                                        if (record.state === ReadingState.IDLE) {
                                            let tmp = section;
                                            proms.push(tmp.save());
                                            section = undefined;

                                        }
                                    } else {
                                        section = new SectionRecord();
                                        section.date = rec.date;
                                        section.page = rec.page;
                                        section.recs = [];
                                        rbModel.sections.push(section._id);
                                    }
                                }
                            });
                            console.log();
                        }
                        if (section) {
                            proms.push(section.save());
                        }
                        proms.push(rbModel.save());
                    }
                });
            }

            // console.log(usr.email);
            // console.log(usr.role);
            console.log();
        })

        // name: String,
        // author: String,
        // pages: Number,
        // usage: Number,
        // user: String,
        // dateCreated: Number,

        // let bookModels = books.map(bInfo => {
        //     let bModel = new Book();
        //     for (const key in bModel) {
        //         if (!bInfo.hasOwnProperty(key)) {
        //             console.log(bInfo);
        //             break;
        //         }
        //     }
        // })
        Promise.all(proms)
          .then(results => {
            console.log(`Finish ${results.length} changes`);
            process.exit();
          }).catch(err => {
              console.log(err);
          })
    })
    .catch(err => {
        console.error("Connection error", err);
    })