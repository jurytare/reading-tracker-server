var path = require('path')
const app = require('./server/app')
const fs = require('fs');

const PORT = process.env.PORT || 8080

app.init(path.join(__dirname, 'client/build'))
// app.setPublic(path.join(__dirname, 'public'))

app.listen(PORT, () => {
 console.log(`Server http is running on port ${PORT}`);
});

// https.createServer({
//   key: fs.readFileSync('./ssl/mrtea.zapto.org/privkey.pem'),
//   cert: fs.readFileSync('./ssl/mrtea.zapto.org/fullchain.pem'),
// }, app).listen(PORT, () => {
//     console.log(`Server https is running on port ${PORT}`);
// })
