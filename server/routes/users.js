var express = require('express');
var router = express.Router();

const auth = require('../middlewares/authJwt');
const userCtrl = require('../controllers/user.controller');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get(
  '/profile/:id', 
  [auth.isOwner],
  userCtrl.accessProfile
)

router.post(
  '/profile/:id',
  [auth.isOwner],
  userCtrl.editProfile
)

module.exports = router;
