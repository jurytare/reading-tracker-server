var express = require('express');
var router = express.Router();

const bookCtrl = require('../controllers/bookStore.controller');
const secCtl = require('../controllers/SecRec.controller');
const BookVerification = require('../middlewares/bookVerification');

router.get('/', bookCtrl.getBookTitles);
router.post('/', bookCtrl.createBook);


router.use('/:bid', BookVerification.checkBookAvailability);

router.get('/:bid', bookCtrl.getBook);
router.post('/:bid', bookCtrl.createRead);

router.get('/:bid/section', secCtl.getSectionsFromBid);

module.exports = router;
