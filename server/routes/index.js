var express = require('express');
var router = express.Router();

const { verifyTokenAndResp } = require('../middlewares/authJwt');

var usersRouter = require('./users');
var signInUpRouter = require('./auth');
var bookStoreRouter = require('./bookStore.route');
var bookShelfRouter = require('./bookShelf.route');

/* GET home page. */
router.get('/index', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/auth', signInUpRouter);

router.use('/', verifyTokenAndResp);

router.use('/users', usersRouter);
router.use('/book', bookStoreRouter);
router.use('/read', bookShelfRouter);

module.exports = router;
