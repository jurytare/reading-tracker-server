var express = require('express');
var router = express.Router();

const checker = require('../middlewares/userVerification')
const authCtl = require('../controllers/auth.controller')

router.post('/signin', authCtl.signin)

router.post(
  '/signup',
  [checker.checkDuplicateUsernameOrEmail],
  authCtl.signup
)

module.exports = router