var express = require('express');
const BookVerification = require('../middlewares/bookVerification');
const secCtl = require('../controllers/SecRec.controller');
var router = express.Router();

// const auth = require('../middlewares/authJwt');
// const userCtrl = require('../controllers/user.controller');

// router.get('/', secCtl.getSection);
router.get('/', secCtl.getAllSections);
router.post('/', secCtl.createSection);
router.use('/:sid/', BookVerification.checkSectionAvailability);

router.put('/:sid/', secCtl.editSection);
router.delete('/:sid/', secCtl.delSection);

// router.post('/:sid/rec/', [BookVerification.extractRecordValue], secCtl.addRecord);
// router.put('/:sid/rec/:rid/', [BookVerification.extractRecordValue], secCtl.editRecord);
// router.delete('/:sid/rec/:rid/', secCtl.delRecord);


module.exports = router;
