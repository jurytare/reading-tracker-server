var express = require('express');
var router = express.Router();

const recCtrl = require('../controllers/record.controller');
const { checkBookHasRecord } = require('../middlewares/dataCheck');

/* GET home page. */
router.get('/', recCtrl.getRecords);
router.post('/', recCtrl.createRecord);

router.use('/:recId', checkBookHasRecord);
router.post('/:recId', recCtrl.updateRecord);
router.delete('/:recId', recCtrl.delRecord);



module.exports = router;
