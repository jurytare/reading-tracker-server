var express = require('express');

const bookCtrl = require('../controllers/bookStore.controller');
var   sectionRouter = require('./SectionRecord.route')
const BookVerification = require('../middlewares/bookVerification');

var router = express.Router();


router.get('/', bookCtrl.getReadList);

router.use('/:rbid', BookVerification.checkReadAvailability);

router.put('/:rbid', bookCtrl.editBook);
router.delete('/:rbid', bookCtrl.delBook);

router.use('/:rbid/section', sectionRouter);

module.exports = router;
