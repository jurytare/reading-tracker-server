function errRes(res, err, obj) {
  res.status(err.code).send({
    msg: err.msg,
    caused: obj,
  });
}

const comErr = {
  ok: { code: 200, msg: 'ok' },
  db: { code: 500, msg: 'Failed to access database' },
  noTok: { code: 403, msg: 'No token provided!' },
  unAuth: { code: 401, msg: 'Unauthorized' },
  noUser: { code: 401, msg: 'Check account information' },
  badReq: { code: 400, msg: 'Unknown command' },
  notFound: { code: 404, msg: 'Resource not found' },
  dup: { code: 409, msg: 'Duplicate resource' },
  conflict: { code: 409, msg: 'Conflict resource' },
  perm: { code: 403, msg: 'Permission denied' },
}

module.exports = {
  errRes,
  comErr,
}