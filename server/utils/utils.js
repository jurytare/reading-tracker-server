function encodeNum(number, base) {
  const code = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  if (!base) {
    base = code.length - 1
  } else if (base >= code.length || base === 1) {
    return -1
  }
  let res = ""
  while (number) {
    res = code[number % base] + res
    number = Math.floor(number / base)
  }
  return res
}

function decodeNum(data, base) {
  const code = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  if (!base) {
    base = code.length - 1
  } else if (base >= code.length || base === 1) {
    return -1
  }
  let result = 0
  let res = 0
  let ch, i = 0;
  while (i < data.length) {
    ch = data.charAt(i)
    res = code.indexOf(ch)
    if (res < 0) {
      console.log("Unable to decode")
      return -1
    } else {
      result = result * base + res
    }
    i++
  }
  return result
}

module.exports = {
  encodeNum,
  decodeNum,
}
