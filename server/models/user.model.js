const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    uid: String,
    email: String,
    password: String,
    role: Number,
  },{autoCreate: true}),
  'Accounts'
);

module.exports = User;
