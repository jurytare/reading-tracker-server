const mongoose = require("mongoose");

const SectionRecord = mongoose.model(
  "SectionRecord",
  new mongoose.Schema({
    rbid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'BookRead',
    },
    date: Number,
    page: Number,
    recs: [{
      time: Number,
      page: Number,
      state: Number,
    }],
  }, { autoCreate: true }),
  'ReadingRecords'
);

module.exports = SectionRecord;
