const mongoose = require("mongoose");

const Book = mongoose.model(
  "Book",
  new mongoose.Schema({
    name: String,
    author: String,
    pages: Number,
    usage: Number,
    user: String,
    dateCreated: Number,
  },{autoCreate: true}),
  'Library'
);

module.exports = Book;
