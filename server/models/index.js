var db = {}
const dbConfig = require('../configs/db.config')
db.mongoose = require('mongoose');

db.init = () => {
  return db.mongoose
  .connect(`${dbConfig.PROTOCOL}://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}${dbConfig.DB_CFG}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(()=> {
    db.mongoose.set('useFindAndModify', false);
  })
}

module.exports = db