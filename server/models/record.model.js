const mongoose = require("mongoose");

const Record = mongoose.model(
  "Record",
  new mongoose.Schema({
    type: Number,
    date: String,
    page: Number,
  }, { autoCreate: true }),
  'records'
);

module.exports = Record;
