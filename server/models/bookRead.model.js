const mongoose = require("mongoose");

const BookRead = mongoose.model(
  "BookRead",
  new mongoose.Schema({
    pages: Number,
    bid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Book',
    },
    user: String,
    sections: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SectionRecord'
      }
    ]
  }, { autoCreate: true }),
  'Bookshelf'
);

module.exports = BookRead;
