const User = require("../models/user.model");
const { comErr, errRes } = require("../utils/errorGenerator");

function checkDuplicateUsernameOrEmail(req, res, next) {
  if(!(req.body.uid && req.body.password && req.body.email)){
    errRes(res, comErr.badReq);
    return;
  }
  User.findOne({ uid: req.body.uid }, (err, user) => {
    if (err) {
      errRes(res, comErr.db);
      return;
    }

    if (user) {
      errRes(res, comErr.dup);
      return;
    }

    // Email
    User.findOne({email: req.body.email}, (err, user) => {
      if (err) {
        errRes(res, comErr.db);
        return;
      }

      if (user) {
        errRes(res, comErr.dup);
        return;
      }

      next();
    });
  });
};

module.exports = {
  checkDuplicateUsernameOrEmail,
}