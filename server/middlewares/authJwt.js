const jwt = require('jsonwebtoken')
const authCfg = require('../configs/auth.config');
const User = require('../models/user.model');
const { comErr, errRes } = require('../utils/errorGenerator');

function authToken(token) {
  return new Promise((resolve, reject) => {
    if(!token) {
      resolve('anonymous');
    }else {
      jwt.verify(token, authCfg.secret, (err, decoded) => {
        if(!err){
          if(decoded.uid) {
            resolve(decoded.uid)
          }else{
            reject(comErr.noUser);
          }
        }else{
          reject(comErr.unAuth);
        }
      });
    }
  });
}

async function verifyTokenAndResp(req, res, next) {
  try {
    let uid = await authToken(req.headers["x-access-token"]);
    User.findOne({uid: uid}, (err, user) => {
      if(err) {
        errRes(res, comErr.db);
      }else{
        if(user) {
          req.userModel = user;
          next();
        }else{
          errRes(res, comErr.unAuth);
        }
      }
    });

  } catch (err) {
    res.status(err.code).send(err.msg);
  }
}

function isOwner(req, res, next) {
    if(req.userModel._id.equals(req.params.id)){
        next()
    }else{
        errRes(res, comErr.perm);
    }
}

module.exports = {
    // authToken,
    verifyTokenAndResp,
    isOwner,
}