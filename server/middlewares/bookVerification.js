const Book = require("../models/book.model");
const BookRead = require("../models/bookRead.model");
const SectionRecord = require("../models/sectionRecord.model");
const { comErr, errRes } = require("../utils/errorGenerator");

function checkBookAvailability(req, res, next) {
  let bookId = req.params.bid || req.query.bid;
  Book.findById(bookId, (err, book) => {
    if(err) {
      errRes(res, comErr.db);
    }else if(!book){
      errRes(res, comErr.notFound);
    }else{
      req.bookModel = book;
      next();
    }
  })
}

function checkReadAvailability(req, res, next) {
  if(!req.userModel) {
    errRes(res, comErr.badReq);
    return;
  }
  let readBookId = req.params.rbid || req.query.rbid;

  BookRead.findById(readBookId, (err, bookRead) => {
    if(err) {
      errRes(res, comErr.db);
    }else if(!bookRead){
      errRes(res, comErr.notFound);
    }else{
      if(bookRead.user === req.userModel.uid) {
        req.readModel = bookRead;
        next();
      }else{
        errRes(res, comErr.perm);
      }
    }
  });
}

function checkSectionAvailability(req, res, next) {
  if(!req.readModel) {
    errRes(res, comErr.badReq);
    return;
  }

  let secId = req.params.sid || req.query.sid;
  
  if(secId){
    if(req.readModel.sections.includes(secId)){
      SectionRecord.findById(secId, (err, sec) => {
        if(err) {
          errRes(res, comErr.db);
        }else if(!sec){
          errRes(res, comErr.notFound);
        }else{
          req.sectionModel = sec;
          next();
        }
      })
    }else{
      errRes(res, comErr.notFound);
    }
  }
}

function extractRecordValue(req, res, next) {
  let record = {
    time: parseInt(req.body.time),
    page: parseInt(req.body.page),
    state: parseInt(req.body.state),
  }

  if(isNaN(record.time)
    || isNaN(record.page)
    || isNaN(record.state)){
      errRes(res, comErr.badReq);
      return;
    }
  req.record = record;
  next();
}

const BookVerification = {
  checkBookAvailability,
  checkReadAvailability,
  checkSectionAvailability,
  extractRecordValue,
}

module.exports = BookVerification;