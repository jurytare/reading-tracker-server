const User = require("../models/user.model")
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const authConfig = require("../configs/auth.config");
const { errRes, comErr } = require("../utils/errorGenerator");

function signin(req, res) {
  let uid = req.body.uid
  if(!(req.body.uid && req.body.password)){
    errRes(res, comErr.badReq);
    return;
  }
  User.findOne({
    uid: uid,
  }, (err, user) => {
    if (err) {
      errRes(res, comErr.badReq);
      return;
    }

    if (!user) {
      errRes(res, comErr.noUser);
      return;
    }

    var passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      errRes(res, comErr.unAuth);
      return;
    }

    let expired = {
      expiresIn: uid === 'tester' ? 86400 * 365 : 86400 // 24 hours
    }
    var token = jwt.sign({ uid: uid }, authConfig.secret, expired);

    res.status(200).send({
      // id: user._id,
      uid: user.uid,
      email: user.email,
      role: user.role,
      accessToken: token,
      expired: new Date().getTime() + expired.expiresIn * 1000,
    });
  })
}

function signup(req, res) {
  const user = new User({
    uid: req.body.uid,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    role: 1,
  });

  user.save((err, user) => {
    if(err) {
      errRes(res, comErr.db);;
      return;
    }
    res.send({ message: "User was registered successfully!" });
  })
}

module.exports = {
  signin,
  signup,
}