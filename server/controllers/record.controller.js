const Book = require("../models/book.model");
const Record = require("../models/record.model");

function getRecords(req, res) {
  const query = Book.findById(req.bookId);
  query.select('rec').populate('rec');
  query.exec((err, book) => {
    if(!err) {
      if(book){
        res.status(200).send(JSON.stringify(book.rec));
        return;
      }
    }
    res.status(500).send({message: "Unable to query records"})
  })
}

function createRecord(req, res) {
  let rec = new Record();
  rec.type = req.body.type;
  rec.date = req.body.date;
  rec.page = req.body.page;

  if((rec.type == null || rec.type == undefined)
    || (rec.date == null || rec.date == undefined)
    || (rec.page == null || rec.page == undefined)){
      res.status(500).send({message: "Mising record information"});
      return;
    }

  rec.save((err, record) => {
    if (err) {
      res.status(500).send({ message: "Unabe create record" });
      return;
    }
    Book.findByIdAndUpdate(req.bookModel._id,
      { $push: { rec: record } },
      (err, book) => {
        if (err) {
          res.status(500).send({ message: "Failed to add rec to book" });
          rec.delete();
          return;
        }
        res.status(200).send(JSON.stringify({recId : record._id}));
      }
    );
  })
}

function updateRecord(req, res) {
  Record.findByIdAndUpdate(req.recId, req.body, (err, record) => {
    if(err) {
      res.status(500).send({message: "Unable to edit record"})
      return;
    }
    res.status(200).send({message: "updateRecord OK"})
  })
}

function delRecord(req, res) {
  Book.findByIdAndUpdate(req.bookId,
    { $pull: { rec: req.recId } },
    (err, book) => {
      if (err) {
        res.status(500).send({ message: "Unable to remove record" })
        return;
      }
      Record.findByIdAndDelete(req.recId, (err, record) => {
        if (err) {
          res.status(500).send({ message: "Warning! Remove uncomplete" });
          return;
        }
        res.status(200).send({ message: "updateRecord OK" })
      })

    })
}

module.exports = {
  getRecords,
  createRecord,
  updateRecord,
  delRecord,
}