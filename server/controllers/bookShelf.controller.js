const Book = require("../models/book.model");
const BookRead = require("../models/bookRead.model");
const SectionRecord = require("../models/sectionRecord.model");
const { comErr, errRes } = require("../utils/errorGenerator");


async function getReadList(req, res) {
  let bookList, query, user;
  
  user = req.userModel;
  try{
    query = BookRead.find({user: user.uid});
    query.select('bid').populate('bid', 'name usage');
    bookList = await query.exec();
    res.status(200).send(bookList.map(book => ({
      _id: book.bid._id,
      name: book.bid.name,
      usage: book.bid.usage,
      rbid: book._id,
    })));
  } catch(err) {
    errRes(res, comErr.db);
    console.log(err);
  }
}

async function addBookRead(req, res) {
  let bid, book, user, bookRead;
  bid = req.params.bid || req.query.bid;
  user = req.userModel;

  if (!bid) {
    errRes(res, comErr.badReq)
    return;
  }

  if (req.bookModel && req.bookModel._id.equals(bid)) {
    book = req.bookModel;
  } else {
    try {
      book = await Book.findById(bid).select('').exec();
      if (!book) {
        errRes(res, comErr.notFound);
        return;
      }

      bookRead = await BookRead.find({
        user: user.uid,
        bid: bid,
      }).select('').exec();
      if (bookRead && bookRead.length) {
        errRes(res, comErr.dup);
        return;
      }
    } catch (err) {
      console.log(err);
      errRes(res, comErr.db)
      return;
    }
  }

  bookRead = new BookRead();
  bookRead.bid = book._id;
  bookRead.user = user.uid;
  try {
    await bookRead.save();
    if(book.usage > 0){
      book.usage++;
    }else{
      book.usage = 1;
    }
    await book.save();
    res.status(201).send({
      ...book.toObject(),
      rbid: bookRead._id,
    });
  }catch (err) {
    bookRead.delete();
    errRes(res, comErr.db);
  }
}

function editBookRead(req, res) {

}

function rmvBookRead(req, res) {
  let user, bookRead;
  user = req.userModel;
  bookRead = req.readModel;
  Promise.all([
    SectionRecord.deleteMany({ _id: { $in: bookRead.sections } }).exec(),
    BookRead.findByIdAndDelete(bookRead._id).exec(),
  ]).then(results => {
    res.status(200).send('ok');
  }).catch(err => {
    errRes(res, comErr.db);
  });
}


const BookReadCtl = {
  getReadList,
  addBookRead,
  editBookRead,
  rmvBookRead,
}

module.exports = BookReadCtl;