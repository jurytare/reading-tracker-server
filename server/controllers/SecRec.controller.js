const BookRead = require("../models/bookRead.model");
const { db } = require("../models/bookRead.model");
const SectionRecord = require("../models/sectionRecord.model");
const { errRes, comErr } = require('../utils/errorGenerator');
const { decodeNum } = require("../utils/utils");

function getAllSections(req, res) {
  req.readModel.populate('sections');
  req.readModel.execPopulate((err, read) => {
    if (err) {
      errRes(res, comErr.badReq);;
    }else {
      res.status(200).send({
        rbid: req.readModel._id,
        pages: read.pages,
        sections: read.sections.map(sec => ({
          _id: sec.id,
          date: sec.date,
          page: sec.page,
          recs: sec.recs,
        }))
      });
    }
  })
}

function getSectionsFromBid(req, res) {
  let book = req.bookModel;
  let user = req.userModel;
  BookRead.findOne({
    bid: book._id,
    user: user.uid,
  }, (err, doc) => {
    if(err) {
      console.log(err);
      errRes(res, comErr.db);
    }else if(!doc) {
      errRes(res, comErr.notFound);
    }else{
      req.readModel = doc;
      getAllSections(req, res);
    }
  })
}

function getSection(req, res, next) {
  if(!req.query.sid) {
    next(); return;
  }
  res.status(200).send({message: "getSection OK"})
}

function createSection(req, res) {
  let readBook = req.readModel;
  let secRec = new SectionRecord();
  secRec.rbid = readBook._id;
  secRec.date = req.body.date || Math.floor(new Date().getTime()/1000);
  secRec.page = req.body.page || 1;
  secRec.recs = [];
  secRec.save((err, section) => {
    if (err) {
      errRes(res, comErr.db);
      return;
    }
    readBook.sections.push(section._id);
    readBook.save((err, bookRead) => {
      if (err) {
        errRes(res, comErr.db);
        secRec.delete();
      }else if(!bookRead) {
        errRes(res, comErr,db);
        secRec.delete();
      }else{
        res.status(201).send(secRec);
      }
    });
  })
}

function delSection(req, res) {
  let section = req.sectionModel;

  req.readModel.sections.pull(section._id);
  req.readModel.save((err, bookRead) => {
    if(err) {
      errRes(res, comErr.db);
    }else{
      section.delete();
      res.status(200).send(section);
    }
  })
}

function editSection (req, res) {
  let secEditReq = req.body;
  let orgSection = req.sectionModel;
  
  // secEditReq.date > 0 && (orgSection.date = secEditReq.date);
  secEditReq.page > 0 && (orgSection.page = secEditReq.page);
  if(Array.isArray(secEditReq.recs)) {
    orgSection.recs = secEditReq.recs.map(rec => {
      typeof rec._id === 'number' && (delete rec._id);
      typeof rec.time !== 'number' && (rec.time = Math.floor(new Date().getTime()/1000) - secEditReq.date);
      return rec
    })
  }

  orgSection.save()
    .then(section => {
      res.status(200).send(section);
    }).catch(err => {
      console.log(err);
      errRes(res, comErr.db);
    });
}

function editSection_futher(req, res) {
  let secEditReq = req.body;
  let orgSection = req.sectionModel;
  let ret = {recs:[]};
  let addList = [];
  console.log('Got req', secEditReq);
  if(typeof secEditReq.page === 'number') {
    orgSection.page = secEditReq.page;
    ret.page = secEditReq.page;
  };
  if(typeof secEditReq.date === 'number') {
    orgSection.date = secEditReq.date;
    ret.date = secEditReq.date;
  };
  if(Array.isArray(secEditReq.recs)) {
    for (const recReq of secEditReq.recs){

      if(typeof recReq._id === 'number' && recReq._id < 0) {
        // This is record adding request
        let rec = {};
        rec.time = typeof recReq.time === 'number' && recReq.time > 0 ? 
          recReq.time : Math.floor(new Date().getTime()/1000) - orgSection.date;  
        rec.state = typeof recReq.state === 'number' ? recReq.state: 1;
        rec.page = recReq.page || 0;
        orgSection.recs.push(rec);
        addList.push({
          reaId: recReq._id,
          ref: orgSection.recs[orgSection.recs.length-1],
          retId: ret.recs.length,
        });
      }else if(typeof recReq._id === 'string') {
        let orgRecId = orgSection.recs.findIndex(rec => rec._id.equals(recReq._id));
        let orgRec = orgSection.recs[orgRecId];

        if(orgRecId >=0 ) {
          if(typeof recReq.state === 'number') {
            // This is record editting request
            orgRec.state = recReq.state;
            orgRec.page = recReq.page;
            orgRec.time = recReq.time;

          }else{
            // This is record deleting request
            orgSection.recs.splice(orgRecId, 1);
          }
        }
      }else{
        console.log("Undefined command for typeof _id " + typeof recReq._id);
        continue;
      }
      ret.recs.push(recReq);
    };
  }

  addList.forEach(rec => {
    rec.modelId = orgSection.recs.indexOf(rec.ref);
  })

  orgSection.save()
    .then((section) => {
      addList.forEach(element => {
        if(element.modelId >= 0) {
          ret.recs[element.retId].resId = section.recs[element.modelId]._id;
          ret.recs[element.retId].page = section.recs[element.modelId].page;
          ret.recs[element.retId].time = section.recs[element.modelId].time;

        }
      })
      res.status(200).send(ret);
    })
    .catch(err => {
        console.log(err);
        errRes(res, comErr.db);
    })
}

function editSection2(req, res) {
  let result, reqAdd = [];
  let secEdition = req.body;
  let secModel = req.sectionModel;
  if(typeof secEdition.crc !== 'string') {
    errRes(res, comErr.badReq);
    return;
  }
  if(secEdition.date) secModel.date = secEdition.date;
  if(secEdition.page) secModel.page = secEdition.page;
  let recData = null;

  for (let i in secEdition.recs) {
    const rec = secEdition.recs[i];
    recData = rec.data ? {
      _id: rec._id,
      time: rec.data[0],
      page: rec.data[1],
      state: rec.data[2],
    } : null;
    if (typeof rec._id === 'string') {
      if (recData) {
        result = editRecord(secModel.recs, recData);
        reqAdd.push({ _id: rec._id, rec: secModel.recs[result.ret] })
      } else {
        result = delRecord(secModel.recs, rec._id);
        reqAdd.push({ _id: rec._id, rec: { _id: 0 } })
      }
    } else if (recData) {
      result = addRecord(secModel.recs, recData);
      reqAdd.push({ _id: rec._id, rec: secModel.recs[result.ret] });
    }
    if (result.ret < 0) {
      console.log('Failed', result, 'at', rec)
      errRes(res, result.reason, rec._id);
      return;
    }
  };
  
  if(verifySection(secModel, secEdition.crc)){
    secModel.save()
      .then(doc => {
        console.log('After editing:', secModel);
        res.status(200).send(reqAdd.map(recRes => {
          return {
            _id: recRes._id,
            rid: recRes.rec._id,
          }
        }));
      })
      .catch(err => {
        console.log(err)
        errRes(res, comErr.db);
      })
  }else{
    errRes(res, comErr.conflict);
  }
}

function addRecord(records, recNew) {

  let i;

  for (i = records.length; i > 0; i--) {
    const rec = records[i-1];
    if(rec.time === recNew.time){
      return {ret: -1, reason: comErr.dup};
    }else if(rec.time < recNew.time) {
      break;
    }
  }
  
  records.splice(i, 0, {
    time: recNew.time,
    page: recNew.page,
    state: recNew.state,
  })
  return {ret: i};
}

function editRecord(records, rec) {
  let i, recEdit, recId = records.findIndex((recOld) => {
    return recOld._id.equals(rec._id);
  });
  if(recId >= 0) {
    recEdit = records[recId];
    recEdit.time = rec.time;
    recEdit.page = rec.page;
    recEdit.state = rec.state;

    for (i = recId; i > 0 && records[i-1].time > recEdit.time; i--);
    if(i!== recId && i >= 0) {
      moveRec(records, recId, i);
      recId = i;
    }else{
      for (i = recId; i < records.length - 1 && records[i+1].time < recEdit.time; i++);
      if(i!=recId && i < records.length) {
        moveRec(records, recId, i);
        recId = i;
      }
    }
  }else{
    console.log('Unable to find rec', rec)
    return {
      ret: -1,
      reason: comErr.notFound,
    }
  }
  
  return {ret: recId};

}

function moveRec(records, id1, id2) {
  let tmp = records.splice(id1, 1);
  records.splice(id2, 0, tmp);
}

function delRecord(records, _id) {
  let id = records.findIndex(oldRec => oldRec._id.equals(_id));
  if(id >= 0) {
    records.splice(id, 1);
    return {ret: id};
  }else{
    return {ret: id, reason: comErr.notFound};
  }
}

function verifySection(section, crcStr) {
  if(typeof crcStr !== 'string') return false;
  let records = section.recs;
  let crcVal = section.date * section.page;
  let crc = decodeNum(crcStr);

  if(!crcVal) {
    return false;
  }
  for (let i = 0; i < records.length; i++) {
    const rec = records[i];
    if (rec.time) {
      if (i < records.length - 1 && rec.state === 0) {
        return false;
      }

      if (i > 0 && rec.page < records[i - 1].page && rec.state) {
        return false;
      }
      crcVal = crcVal * 2 + (rec.time ? rec.time : 1) * (rec.page ? rec.page : 1);
    } else {
      return false;
    }
  }
  // console.log('Compare', crcVal, 'vs', crc);
  return crcVal === crc;
}

function verifyRecord(records, id, rec) {
  if(id < records.length-1) {
    if(rec.time >= records[id+1].time) {
      return false;
    }
    if(rec.state === 0 && rec.page > 0) {
      // Unable to set stop at middle of records
      return false;
    }
  }
  if(id > 0) {
    if(rec.time <= records[id-1].time) {
      return false;
    }
  }
  return true;
}

const SecRecCtl = {
  getAllSections,
  getSection,
  createSection,
  delSection,
  editSection,

  getSectionsFromBid,
}

module.exports = SecRecCtl;

