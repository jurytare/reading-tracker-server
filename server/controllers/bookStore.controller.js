const { comErr, errRes } = require('../utils/errorGenerator');
var Book = require('../models/book.model')
const BookRead = require('../models/bookRead.model');
const SectionRecord = require('../models/sectionRecord.model');


function bookBuilder(book, user, name, pages, author) {
  book.name = name;
  book.pages = pages;
  book.user = user;
  book.author = author;
  book.usage = 0;
  book.dateCreated = Math.floor(new Date().getTime()/1000);
}

async function checkDuplicateBook(name, pages) {
  let bookCheck = await Book.findOne({
    name: name,
    pages: pages,
  }).select('').exec();
  return bookCheck? true:false;
}

async function getBookTitles(req, res) {
  let numberOfTitles = 10;
  let query, fieldQuery = 'name usage';
  let requestNumber = Number(req.query.n)
  if(requestNumber && requestNumber < numberOfTitles) {
    numberOfTitles = requestNumber < 20 ? requestNumber: 20;
  }

  query = Book.find({});
  query.select(fieldQuery);
  query.sort({'usage': -1});
  query.limit(numberOfTitles);
  try{
    const doc = await query.exec();
    res.status(200).send(doc);
  } catch (err) {
    console.log(err);
    errRes(res, comErr.db);
  }
}

async function getReadList(req, res) {
  let bookList, query, user;
  
  user = req.userModel;
  try{
    query = BookRead.find({user: user.uid});
    query.select('bid').populate('bid', 'name usage');
    bookList = await query.exec();
    res.status(200).send(bookList.map(book => ({
      _id: book.bid._id,
      name: book.bid.name,
      usage: book.bid.usage,
      rbid: book._id,
    })));
  } catch(err) {
    errRes(res, comErr.db);
    console.log(err);
  }
}

async function getBook(req, res) {
  let bid = req.query.bid || req.params.bid;
  let book, bookRead;

  res.status(200).send(req.bookModel);

  return;

  try {
    book = await Book.findById(bid).exec(); 

    if(book) {
      bookRead = await Book.findOne({
        bid: bid,
        user: req.userModel.uid,
      }).exec();
      if(bookRead) {
        return res.status(200).send({
          ...book.toObject(),
          rbid: bookRead._id,
        })
      }else{
        return res.status(200).send(book);  
      }
    }else{
      return errRes(res, comErr.notFound);
    }
  } catch (err) {
    console.log(err);
    errRes(res, comErr.db);
  }
}

function getMyBook(req, res) {
  let bid = req.query.bid || req.params.bid;
  BookRead.findOne({
    bid: bid,
    user: req.userModel.uid,
  }, (err, doc) => {
    if(err) {
      console.log(err);
      errRes(res, comErr.db);
    }else{
      if(doc){
        res.status(200).send(doc);
      }else{
        errRes(res, comErr.notFound);
      }
    }
  })
}

async function createBook(req, res) {
  let book;
  let {name, pages, author} = req.body;

  if (!(name && pages)) {
    errRes(res, comErr.badReq)
    return;
  }

  try {
    if (await checkDuplicateBook(name, pages)) {
      errRes(res, comErr.dup);
      return;
    }
  } catch (err) {
    console.log(err);
    errRes(res, comErr.db);
    return;
  }

  book = new Book();
  bookBuilder(book, req.userModel.uid, name, pages, author);
  book.usage = 0;
  req.bookModel = book;
  createRead(req, res);
}

async function createRead(req, res) {
  let book, user;
  book = req.bookModel;
  user = req.userModel;
  BookRead.findOne({
    bid: book._id,
    user: user.uid,
  }, (err, doc) => {
    if(err) {
      console.log(err);
      errRes(res, comErr.db);
    }else if(doc) {
      errRes(res, comErr.dup);
    }else {
      let read = new BookRead();
      read.pages = book.pages;
      read.bid = book._id;
      read.user = user.uid;
      read.sections = [];
      book.usage++;
      book.save();
      read.save()
       .then(rb => {
         res.status(200).send({
           ...book.toObject(),
           rbid: rb._id,
         });
       }).catch(err => {
         console.log(err);
         errRes(res, comErr.db);
       })
    }
  })

}

async function editBook(req, res) {
  let readBook, user, name, pages, author;

  user = req.userModel;
  readBook = req.readModel;

  if(await checkDuplicateBook(name, pages)) {
    errRes(res, comErr.dup);
    return;
  }

  try {
      let editedBook, book;
      book = await Book.findById(readBook.bid).exec();
      name = req.body.name || book.name;
      pages = req.body.pages || book.pages;
      author = req.body.author || book.author;
    
      if(book) {
        if(book.usage === 1){
          book.name = name;
          book.pages = pages;
          book.author = author;
          await book.save();
          editedBook = book;
        }else{
          book.usage--;
          book.save();
          editedBook = new Book();
          bookBuilder(editedBook, user.uid, name, pages, author);
          editedBook.usage = 1;
          await editedBook.save();
          readBook.bid = editedBook._id;
          readBook.save();
        }
        res.status(200).send({
          ...editedBook.toObject(),
          rbid: readBook._id,
        });
        return;
      } else {
        errRes(res, comErr.notFound);
      }
  } catch (err) {
    console.log(err);
    errRes(res, comErr.db)
  }
}

function delBook(req, res) {
  let readBook = req.readModel;

  let proms = [];

  Book.findById(readBook.bid).select('').exec()
    .then(book => {
      if(book.usage > 1){
        book.usage--;
        proms.push(book.save());
      }else{
        proms.push(Book.findByIdAndDelete(book._id));
      }
      proms.push(SectionRecord.deleteMany({ _id: { $in: readBook.sections } }).exec());
      proms.push(BookRead.findByIdAndDelete(readBook._id).exec())

      Promise.all(proms);
      res.status(200).send('ok');

    }).catch(err => {
      console.log(err);
      errRes(res, comErr.db);
    })
}

module.exports = {
  getBookTitles,
  getReadList,
  getBook,
  createBook,
  createRead,
  editBook,
  delBook,
}